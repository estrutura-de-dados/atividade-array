
Filter:
    Filter vai filtrar os elementos da lista de acordo como for definido, adicionando em uma lista 
    o que passar no seu filtro.

ForEach:
    ForEach chama uma função para todos os elementos da matriz um de cada vez.

Join:
    Esse metodo ira criar uma string juntando todos os elementos do array, separando pelo que
    for definido que pode ser por exemplo " ", por padrão ele usa virgula.

lastIndexOf:
    esse metodo percorrera uma string e retornara a posição da ultima vez que o parametro
    passado ocorreu, se não ocorrer retornara -1.

indexOf: 
    Esse metodo percorre uma string e retorna a posição da primeira vez que o parametro passado ocorreu,
    se não ocorrer retornara -1.

map:
    O metodo map cria uma nova matriz preenchida com os resultados da chamada de uma função fornecida
    em cada elemento da matriz de chamada.

reverse:
    inverte o array de tras para frente

slice:
    O slise pegara os objetos de uma lista de apordo com as posiçoes que lhe forem passadas
    e adiciona em um novo array, poren não pega a ultima posição passada, por exemplo
    se for passado como paramentro (1, 3) ele pegara a segunda e a terseira posição, não pegara a quarta. 

some:
    Verifica e retorna um boolean de acordo com a verificação imposta.


sort:
    Organisa os elementos de uma matriz em ordem alfabética


concat:
    Concatena duar matrizes.


every:
    Verifica se todos os elementos da matriz passam na verificação imposta, retornando um boolean.



