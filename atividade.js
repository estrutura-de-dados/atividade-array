
//Filter

const animais = ['cachorro', 'gato', 'ovelha', 'tijolo'];

const resultado = animais.filter(animais => animais.length <= 4);

console.log(resultado);


//ForEach

var soma = 0;
var matriz = [5, 9, 10, 3, 6];
matriz.forEach(somar);

function somar(item) {
  soma += item;
}

//join

var componentes = ['processador', 'placa mãe', 'placa de video', 'memoria'];
var pc = componentes.join(", ");


//lastIndexOf

var mouse = "O mouse estragou por que estava muito velho";
var indise = mouse.lastIndexOf("velho");


//indexOf

var dose = "chocolate é dose por que é chocolate";
var indise = dise.indexOf("chocolate");

//map

const numeros = [1, 4, 6, 10];


const map = numeros.map(x => x * 5);

console.log(map);

//reverse

var animais = ['cachorro', 'gato', 'ovelha', 'tijolo'];
animais.reverse();

//slice

var carros = ["gol", "corsa", "chevete", "sandero"];
var garagen = carros.slice(1, 3);

//some

var idades = [3, 10, 18, 20];

function verificaDeMaior(idade) {
  return idade >= 18;
}

var maior = idades.some(verificaDeMaior);

//sort

var nomes = ["Jubileu", "Pedro", "Larissa", "Fernanda"];
nomes.sort();

console.log(nomes);

//concat

var nomes1 = ["Amanda", "Lucas"];
var nomes2 = ["Juliano", "Alberto", "Marcia"];
var listaCompletaNomes = nomes1.concat(nomes2);

//every

var idades = [32, 33, 16, 40];

function verificaTodosDeMaior(idade) {
  return idade >= 18;
}

var todosAdultos = idades.every(verificaTodosDeMaior);